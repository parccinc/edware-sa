#!/bin/bash

set -e
##
# USAGE INSTRUCTIONS AND PREREQUISITES INFO
# ------------------------------------------------------------------------------
#   Copy and Paste this script onto the instance to become the openam-server
#    (suggested location of script file on target host is /root/<scriptname> )
#   
#   PreRequisite:  you must have a copy of the ldif file "edwareTestUsers.ldif" pulled from the edware git repo
#       copied on to the target host into the /tmp dir (with same file name), BEFORE YOU CAN RUN THIS SCRIPT
#   
#   Configure the hostname values prior to running script.  This includes do the following:
#       1) export HOSTNAME=post-uat-openam.dev.parccresults.org
#       2) hostname post-uat-openam.dev.parccresults.org
#       3) append the value "post-uat-openam.dev.parccresults.org" (for example,) to the /etc/hosts file for the first line 
#               (e.g.  127.0.0.1  localhost ..... post-uat-openam.dev.parccresults.org )
#           and also as the last line of /etc/hosts with the private IP address + "post-uat-openam.dev.parccresults.org" (for example)
#              (e.g.  10.x.x.x  post-uat-openam.dev.parccresults.org )
#       4) edit the /etc/resolv.conf by making the "search" line have "<env>.parccresults.org" as first domain then the original listed second
#              (e.g. = search dev.parccresults.org ec2.internal  )
#       5) AFTER ALL THE STEPS 1 thru 4 are completed, THEN run this script    (!! IMPORTANT !!)
#
##

CURR_HELPER_SCRIPT_DIRPATH=$(dirname $0)
cd ${CURR_HELPER_SCRIPT_DIRPATH}
CURR_WORKING_DIR=$(pwd)

CURR_ENV=dev
CURR_HOSTNAME=post-uat-openam.${CURR_ENV}.parccresults.org

EC2_INSTANCE_PROFILE_ROLENAME=nonOrchestratorEC2Role
if [[ ! -e /usr/local/aws ]] ; then
    wget 'https://s3.amazonaws.com/aws-cli/awscli-bundle.zip'
    unzip -d /tmp ./awscli-bundle.zip
    /tmp/awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws
    rm -rf /tmp/awscli-bundle
    rm awscli-bundle.zip
    
    aws configure set region us-east-1
    curl -s "http://169.254.169.254/latest/meta-data/iam/security-credentials/$EC2_INSTANCE_PROFILE_ROLENAME"
fi

# Check if the required YUM packages have been installed 
# ( and do NOT run install again if it has been)
if [[ -z `yum list installed | grep -o "httpd"` ]] ; then
    yum install -y httpd 
fi
if [[ -z `yum list installed | grep -o "java-1.7.0-openjdk"` ]] ; then
    yum install -y java-1.7.0-openjdk
fi
if [[ -z `yum list installed | grep -o "tomcat6-webapps"` ]] ; then
    yum install -y tomcat6-webapps
fi

# Now we are Downloading All Dependencies required for installing OpenAM & OpenDJ 
if [[ ! -e "$CURR_HELPER_SCRIPT_DIRPATH/srcfiles_for_openam-sso_installing/98-sbac.ldif" ]] ; then
    mkdir ${CURR_HELPER_SCRIPT_DIRPATH}/srcfiles_for_openam-sso_installing
    aws s3 cp s3://amplify-parcc-devops-utils-stg/srcfiles_for_openam-sso_installing/ ${CURR_HELPER_SCRIPT_DIRPATH}/srcfiles_for_openam-sso_installing/ --recursive
fi

if [[ ! -e "/opt/OpenDJ-2.4.6/" ]] ; then
    unzip -d /opt ${CURR_HELPER_SCRIPT_DIRPATH}/srcfiles_for_openam-sso_installing/opendj-2.4.6.zip
    cp ${CURR_HELPER_SCRIPT_DIRPATH}/srcfiles_for_openam-sso_installing/98-sbac.ldif /opt/OpenDJ-2.4.6/config/schema/
    cp ${CURR_HELPER_SCRIPT_DIRPATH}/edwareTestUsers.ldif /tmp/edwareTestUsers.ldif
    
    /opt/OpenDJ-2.4.6/setup \
              --cli \
              --baseDN ou=environment,dc=edwdc,dc=net \
              --ldifFile /tmp/edwareTestUsers.ldif \
              --ldapPort 2389 \
              --adminConnectorPort 5444 \
              --rootUserDN cn=Manager,ou=environment,dc=edwdc,dc=net \
              --rootUserPassword secret \
              --enableStartTLS \
              --ldapsPort 2636 \
              --generateSelfSignedCertificate \
              --hostName ${CURR_HOSTNAME} \
              --no-prompt \
              --noPropertiesFile
    
    /sbin/iptables -A INPUT -p tcp -m state --state NEW --dport 2636 -j ACCEPT
    /etc/rc.d/init.d/iptables save
    
    cd /opt/OpenDJ-2.4.6/config/
    keytool -export -alias "server-cert" -keystore keystore -storepass `cat keystore.pin` -file /tmp/server-cert.cer
    cd /usr/share/tomcat6/conf/
    keytool -import -v -trustcacerts -alias "server-cert"  -keystore truststore -keypass changeit  -file /tmp/server-cert.cer
    /sbin/iptables -A INPUT -p tcp -m state --state NEW -m tcp --dport 8443 -j ACCEPT
    /etc/rc.d/init.d/iptables save
fi

cp ${CURR_HELPER_SCRIPT_DIRPATH}/srcfiles_for_openam-sso_installing/forgerock_software_orig_archive_files/OpenAM-11.0.0.war /usr/share/tomcat6/webapps/openam.war

if [[ ! -e "/opt/openam/" ]] ; then
    mkdir /opt/openam/
    chmod 775 /opt/openam
    chown -R tomcat:tomcat /opt/openam/
fi

chmod -R 777 /usr/share/tomcat6/

service tomcat6 start

echo "done with script now"
