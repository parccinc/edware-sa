#!/bin/bash

set -e
##
# USAGE INSTRUCTIONS AND PREREQUISITES INFO
# ------------------------------------------------------------------------------
#   Copy and Paste this script onto the vpc-parcc-jmpbx in the "root" user's home dir
#   Make the script executable if not already
#
#   1) call the script from terminal as root user, passing the Usernames as Args (the Amplify Login username, not with "a-" prefix)
#   2) the ARGS are space separated username values if creating more than one new user account
#   3) NOTE:  modify the value for "PUBKEY_CURRENV" appropriately for the ENV Acct, and use ALL UPPERCASE LETTERS
#      (i.e.  DEV, QA, STG, or PRD )
#
##
PUBKEY_DEV="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDmYR8FJxLhut54ZNXr6zZ8iYBxJ7JvwFBa8ZMs28SJtzwk6A6nSBnJdcVVfUy6AbSHohYjWYin7wEu3RwysLnOxrmuhIL/ytglLRrFzblrNYNZvVi0Vufn/1TgYh98tN//dFgBIOE/f5fb+PH6QK8tOS+FVM5zL3yy1OcoZENQ4VzDhTRZx1F71BRwCiTLXBUBZcYgor08xGp8RtpDm60kZ5D/J/Afe2PynZoT4vs8CIHUltGzRpasxSpPlSjHx6NsbnFDYHqI79lFqA5p4/fhF0ksnVNvgcCdB8z7Aq8D8wrvfrxkFEOMNjNwu46OfrTayys6MNurrtxvzQnJDYnJ"
PUBKEY_QA="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCy0fVxMGmSSrRcet2KooaqZ0KtDM9IrIeygD0Oz6Rx/buMpviDwAM2y2eChk5BOsGKxGkwSWEMB9a0QlFINsvtObl40UzyDNDmByh3faD5V72n8GYpWiKUOXFN+VT6ZQfogESPkwyuSMHEt8fP2za/fGetPy6iju8L5nVRk3fKATs4uvibN91/vdNU/fITc44EFmvCJIyD//mDCtLBW775IR0x2T+AFj6eSVP4xhCTpAnUuwX9kSfC5+CqphQ5KfMUpoEebpSuRJNtlFFaqbX7CiW9ns9L5US9DrQhxYph8a8enBPJyO+blBlF1A3BKKDEbEriOueZn0CqmeYjZTUv"
PUBKEY_STG="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCdNuUqOWwTRx+auVC8LmDwLWHE4Atpvq1/BB8ev8Ln+BCo2txyRexNi4xZtvY7oGkz5IMZb0/7OgaJ8DMYyTixV62sAXCNtyl8FLZfBAsBOPa8vP6c8k0x+VtFPkC1NOadXr6jNB5yXiJsAofYqLtfFxGZfwQUM6ZLhhCHVnpB54elon2N/L6YoEW5t9YnIox0qLm2b0SRdx+78Rayyeo+fixZ14CAMTIdjzsJ84dbU92ZfAgX7Ub0GnM6S+F8iXFDiI2e0P0JTgZ8PXt+de3b3RR7zO28vN8zl4O7AhJcT5seWqEuN3WodjswwncLp35QkiopjynJi3saCroj3tgr"
PUBKEY_PRD="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCMcLLBbJR1HtueaKHEMS1E4VuHsfps7doq4NLEoelFvNA3htQASVegh/2talKkV36o8vU0T94INX90jqO+ewkrtFtRGIHzivMFBviqSbzl55HTQcB6czuq/ahbwdtCJv2mwdOWacFQRYkt7PSKISx6HiHSNcOnmBMaeQTmCxG0PzVjb7xjI7XmtjHc0cHXQab5ZOJ89bAKUczMBaOSy6CuAidTnNLimtELA6Rtj9bOrwo8y2sJ4ZtDJm1Nz68+IuXLoR8z8BlCH5IRE8gZCSjj9aU9QPzrLZbQeMiN8AbtCzdW6A7c6N8fjthbV8bjUQukR8iETGYcaKLEAcSDL3Mb"

PUBKEY_CURRENV=PUBKEY_QA

NEWUSERS="${@}"
for newusername in ${NEWUSERS}
do
    useradd a-${newusername}
    mkdir /home/a-${newusername}/.ssh
    chmod 0700 /home/a-${newusername}/.ssh
    chown a-${newusername}:a-${newusername} /home/a-${newusername}/.ssh
    echo "${!PUBKEY_CURRENV}" >> /home/a-${newusername}/.ssh/authorized_keys
    echo "a-${newusername}  ALL=(ALL)   NOPASSWD: ALL" | (EDITOR="tee -a" visudo)
    
    echo "Finished steps for a-${newusername}"
done
	