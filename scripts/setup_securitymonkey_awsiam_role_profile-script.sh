#!/bin/bash

CURR_DIR=`dirname $0`

aws_acct_env=qa

#ROLE_NAME=SecurityMonkey
#IAMACCESS_POLICY_FILENAME=SecurityMonkeyReadOnly
ROLE_NAME=SecurityMonkeyInstanceProfile
ROLE_INSTANCE_PROFILE_NAME=SecurityMonkeyInstanceProfile
TRUST_POLICY_FILENAME=ec2role-trust-policy
IAMACCESS_POLICY_FILENAME=SecurityMonkeyLaunchPerms

if [[ -n "$aws_acct_env" ]] ; then
    export AWS_DEFAULT_PROFILE=${aws_acct_env}-egaluskin
    export AWS_PROFILE=${aws_acct_env}-egaluskin
fi

aws iam create-role --role-name "$ROLE_NAME" --assume-role-policy-document file://"$CURR_DIR"/iam_policy_files/"$TRUST_POLICY_FILENAME".json
aws iam put-role-policy --role-name "$ROLE_NAME" --policy-name "$IAMACCESS_POLICY_FILENAME" --policy-document file://"$IAMACCESS_POLICY_FILENAME".json
aws iam create-instance-profile --instance-profile-name "$ROLE_INSTANCE_PROFILE_NAME"
aws iam add-role-to-instance-profile --instance-profile-name "$ROLE_INSTANCE_PROFILE_NAME" --role-name "$ROLE_NAME"


