def get_attribute_from_list(list_of_items, attribute):
    return [x.get(attribute) for x in list_of_items]


class FilterModule(object):
    def filters(self):
        return {
            'get_attribute_from_list': get_attribute_from_list,
        }
