---
# RDS Subnet group requires subnets in at least 2 availability zones
- name: Check if RDS subnet exists
  command: aws ec2 describe-subnets \
    --filters "Name=cidr-block,Values={{rds_backup_subnet_vpc_cidr}}" \
    --query Subnets[0].SubnetId
  register: rds_subnet_check

- name: Create RDS subnet for backup zone
  command: aws ec2 create-subnet \
    --vpc-id '{{vpc_id}}' --cidr-block '{{rds_backup_subnet_vpc_cidr}}' \
    --availability-zone '{{rds_backup_zone}}'
  when: rds_subnet_check.stdout == ""

- name: Get aws environment subnet id value
  command: aws ec2 describe-subnets \
    --filters "Name=cidr-block,Values={{item}}" \
    --query "Subnets[*].SubnetId" \
    --output=text
  register: curr_env_subnet_ids
  with_items:
    - '{{pvt_subnet_vpc_cidr}}'
    - '{{rds_backup_subnet_vpc_cidr}}'

- name: Tag the new RDS subnet
  command: aws ec2 create-tags \
    --resources '{{curr_env_subnet_ids.results[1].stdout}}' \
    --tags Key=\"{{item.key}}\",Value=\"{{item.value}}\"
  with_dict: rds_subnet_tags
  when: rds_subnet_check.stdout == ""

- name: Get the route table from the private subnet
  command: aws ec2 describe-route-tables \
    --filters Name=association.subnet-id,Values='{{curr_env_subnet_ids.results[0].stdout}}' \
    --query RouteTables[*].RouteTableId --output=text
  register: private_route_table
  when: rds_subnet_check.stdout == ""

- name: Associate the RDS subnet with the private route table
  command: aws ec2 associate-route-table \
    --subnet-id '{{curr_env_subnet_ids.results[1].stdout}}' \
    --route-table-id '{{private_route_table.stdout}}'
  when: rds_subnet_check.stdout == ""

- name: Create RDS subnet group
  rds_subnet_group:
    state: present
    name: '{{rds_subnet_group_name}}'
    description: '{{rds_subnet_group_desc}}'
    region: '{{region}}'
    subnets: '{{curr_env_subnet_ids.results | get_attribute_from_list("stdout") }}'
