# README Instructructions
To run these playbooks, cd into this repo's ansible_playbooks directory and run the following command:
<code>
ansible-playbook -i inventory/localhost -e env=<AWS_ENV> create-iam-entities.yml
</code>

* Note: replace "create-iam-entities.yml" with other playbooks as needed
* NOTE: you MUST make sure your terminal session is using the appropriate aws_acct_environment's credentials (i.e. export AWS_DEFAULT_PROFILE=<env>)

# DRMS LICENSE #
Copyright © <2014 - 2016> <Parcc, Inc.>

Copyright © <2014 - 2016> <Amplify Education, Inc.>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the 

Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.